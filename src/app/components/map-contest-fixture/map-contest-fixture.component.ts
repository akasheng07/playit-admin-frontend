import { Component, OnInit } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { ContextService } from 'src/app/services/context.service';
import { MapcontestfixtureService } from 'src/app/services/mapcontestfixture.service';
import { MatchfixtureService } from 'src/app/services/matchfixture.service';



interface TableRow{
  id: number;
  name: string;
  desc: string;
}

@Component({
  selector: 'app-map-contest-fixture',
  templateUrl: './map-contest-fixture.component.html',
  styleUrls: ['./map-contest-fixture.component.css']
})
export class MapContestFixtureComponent implements OnInit {

  rows: TableRow[] = [];
  editingRowId: number | null = null;
  matchfixtures:any;
  contests:any;
  mapcontestfixturedata:any;

  
  public mapContestFixture={
    "matchid":"",
    "matchname":"",
    "contestid":"",
    "contestname":""
  };
 
  

  constructor(private _matchFixtureService:MatchfixtureService,
    private _contextService:ContextService,
    private _mapContestFixtureService:MapcontestfixtureService) { }

  ngOnInit(): void {
    this.loadMatFixtureData();
    this.loadContestData();
    this.loadMapContestFixtureData();
  }
  

  alreadyMappedContestFixture:any[] | undefined;
  filteredContestId:any[] | undefined;
  filteredContests:any[] | undefined;
  clicked1(event:MatSelectChange){

    // get the selected matchid from select option
    const selectedMatchId=event.value;
    // console.log(`Selected Match Id : ${selectedMatchId}`);

    // find all the connected (already mapped) contestid with that selected matchid
    this.alreadyMappedContestFixture = this.mapcontestfixturedata.filter((item: { matchid: any; }) => item.matchid===selectedMatchId);
    // console.log("matchfixture data of selected match : ")
    // console.log(this.alreadyMappedContestFixture);

    // console.log("contests id of already mapped contest with selected id :")
    this.filteredContestId = this.alreadyMappedContestFixture?.map(item=>item.contestid);
    // console.log(this.filteredContestId);


    // filter out the rest contestid and display it to the contest select option
    this.filteredContests = this.contests.filter((c: { id: any; })=>!this.filteredContestId?.includes(c.id));
    // console.log("Filtered Contests : ");
    // console.log(this.filteredContests);
    
   

  }

 



  loadMapContestFixtureData(){
    this._mapContestFixtureService.getAll().subscribe(
      (data)=>{
        this.mapcontestfixturedata=data;
        // console.log(this.mapcontestfixturedata)
      },
      (error)=>{console.log(error)}
    )
  }

  loadMatFixtureData(){
    this._matchFixtureService.getAll().subscribe(
      (data)=>{
        this.matchfixtures = data;
      
        
      },
      (error)=>{
        console.log(error);
      }
    )
  }

  loadContestData(){
    this._contextService.getAll().subscribe(
      (data)=>{
        this.contests=data;
      },
      (error)=>{console.log(error)}
    )
  }

  add(){

    


    this._mapContestFixtureService.create(this.mapContestFixture).subscribe(
      (data)=>{console.log(data)
      alert("Mapped Successfully...")
      },
      (error)=>{console.log(error)}
    )
  }

  delete(id:any){
    this._mapContestFixtureService.delete(id).subscribe(
    (data)=>{console.log(data)},
    (error)=>{console.log(error)}
    )
  }

  addRow(){

    const newRow: TableRow = {
      id: this.rows.length + 1,
      name:'',
      desc:'',
    };
    
    this.rows.push(newRow);
  }

  editRow(row: TableRow){
    this.editingRowId = row.id;
  }



}
