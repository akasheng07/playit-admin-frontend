import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MapcontestfixtureService {
  private baseUrl= "http://localhost:8080/playIt/api/vi/mapcontestfixture";
  
  constructor(private _http:HttpClient) { }

  
  //create 
  public create(mapcontestfixture:any){
    return this._http.post(`${this.baseUrl}/`,mapcontestfixture);
  }
  
  //to get all the teams 
  public getAll(){
    return this._http.get(`${this.baseUrl}/`);
  }

  //delete player
  public delete(id:any){
    return this._http.delete(`${this.baseUrl}/${id}`);
  }

  
}
