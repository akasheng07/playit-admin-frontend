import { TestBed } from '@angular/core/testing';

import { MapcontestfixtureService } from './mapcontestfixture.service';

describe('MapcontestfixtureService', () => {
  let service: MapcontestfixtureService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MapcontestfixtureService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
